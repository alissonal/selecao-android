package com.alisson.lectureorganizer.presenter;

import org.junit.Test;

import java.time.Duration;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ConferenceTest {
    @Test
    public void isConferenceNullUnitTest() {
        assertEquals(false, (Conference.getInstance() == null));
    }

    @Test
    public void conferenceOrganizedTest() {
        ArrayList<Lecture> lecturesToOrganize = new ArrayList<Lecture>();
        Lecture l1 = new Lecture(
                "Palestra a",
                Duration.ofMinutes(60));
        Lecture l2 = new Lecture(
                "Palestra b",
                Duration.ofMinutes(45));
        Lecture l3 = new Lecture(
                "Palestra c",
                Duration.ofMinutes(30));
        Lecture l4 = new Lecture(
                "Palestra d",
                Duration.ofMinutes(30));
        Lecture l5 = new Lecture(
                "Palestra c",
                Duration.ofMinutes(30));


        lecturesToOrganize.add(l1);
        lecturesToOrganize.add(l2);
        lecturesToOrganize.add(l3);
        lecturesToOrganize.add(l4);
        lecturesToOrganize.add(l5);

        Conference.setLecturesToOrganize(lecturesToOrganize);

        assertEquals(true, Conference.organizeConference());
    }

    @Test
    public void conferenceOrganizedTest2() {
        ArrayList<Lecture> lecturesToOrganize = new ArrayList<Lecture>();
        ArrayList<Lecture> lecturesToOrganize2 = new ArrayList<Lecture>();

        Lecture l1 = new Lecture(
                "Palestra a",
                Duration.ofMinutes(180));
        Lecture l2 = new Lecture(
                "Palestra b",
                Duration.ofMinutes(239));
        Lecture l3 = new Lecture(
                "Palestra c",
                Duration.ofMinutes(180));
        Lecture l4 = new Lecture(
                "Palestra d",
                Duration.ofMinutes(239));
        Lecture l5 = new Lecture(
                "Palestra e",
                Duration.ofMinutes(1));

        lecturesToOrganize.add(l1);
        lecturesToOrganize.add(l2);
        lecturesToOrganize.add(l3);
        lecturesToOrganize.add(l4);

        lecturesToOrganize2.addAll(lecturesToOrganize);
        lecturesToOrganize2.add(l5);

        Conference.setLecturesToOrganize(lecturesToOrganize);
        assertEquals(true, Conference.organizeConference());

        Conference.setLecturesToOrganize(lecturesToOrganize2);
        assertEquals(false, Conference.organizeConference());
    }
}
