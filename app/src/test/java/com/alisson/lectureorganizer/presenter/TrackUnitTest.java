package com.alisson.lectureorganizer.presenter;

import org.junit.Test;

import java.time.Duration;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class TrackUnitTest {
    @Test
    public void isTrackInstantiateNullTest() {
        Track t = new Track("Teste");
        assertEquals(false, (t == null));
    }

    @Test
    public void moveLecturesToTrackTest() {
        Track t = new Track("Teste");
        ArrayList<Lecture> lecturesToOrganize = new ArrayList<Lecture>();
        ArrayList<Lecture> morningLectures = new ArrayList<Lecture>();
        ArrayList<Lecture> afternoonLectures = new ArrayList<Lecture>();

        Lecture l1 = new Lecture(
                "Palestra a",
                Duration.ofMinutes(60));
        Lecture l2 = new Lecture(
                "Palestra b",
                Duration.ofMinutes(45));
        Lecture l3 = new Lecture(
                "Palestra c",
                Duration.ofMinutes(30));
        Lecture l4 = new Lecture(
                "Palestra d",
                Duration.ofMinutes(30));
        Lecture l5 = new Lecture(
                "Palestra c",
                Duration.ofMinutes(30));
        Lecture l6 = new Lecture(
                "Palestra c",
                Duration.ofMinutes(60));

        morningLectures.add(l1);
        morningLectures.add(l2);
        morningLectures.add(l3);
        morningLectures.add(l4);

        afternoonLectures.add(l5);
        afternoonLectures.add(l6);

        lecturesToOrganize.addAll(morningLectures);
        lecturesToOrganize.addAll(afternoonLectures);

        t.moveLecturesToTrack(lecturesToOrganize);

        assertEquals(morningLectures, t.getMorningSession().getLecturesSession());
        assertEquals(afternoonLectures, t.getAfternoonSession().getLecturesSession());
    }
}
