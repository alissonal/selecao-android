package com.alisson.lectureorganizer.presenter;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilsTest {
    @Test
    public void stringHaveNumbersTest() {

        boolean stringDontHaveNumber = StringUtils.doesStringHaveNumbers(
                "Não tem número");
        boolean stringHaveNumber = StringUtils.doesStringHaveNumbers(
                "Tem 1 número");
        assertEquals(false, stringDontHaveNumber);
        assertEquals(true, stringHaveNumber);
    }

    @Test
    public void splitLinesTest() {
        String testString = "Essa é uma linha\nEssa é outra linha";
        String testString2 = "Essa é uma linha";
        String testString3 = "Essa é outra linha";
        String[] testStringArray = StringUtils.splitLines(testString);
        assertEquals(testString2, testStringArray[0]);
        assertEquals(testString3, testStringArray[1]);
    }

    @Test
    public void separateLastWordFromStringTest() {
        String testString = "Esse é um teste como outros";
        String testString2 = "Esse é um teste como";
        String testString3 = "outros";
        String[] testStringArray = StringUtils.separateLastWordFromString(testString);
        assertEquals(testString2, testStringArray[0]);
        assertEquals(testString3, testStringArray[1]);
    }
}
