package com.alisson.lectureorganizer.presenter;


import java.time.Duration;
import java.util.ArrayList;

import org.junit.Test;
import static org.junit.Assert.*;

public class SortLecturesTest {
    @Test
    public void testSortLectures() {
        ArrayList<Lecture> lecturesToOrganize = new ArrayList<Lecture>();
        ArrayList<Lecture> lecturesToOrganize2 = new ArrayList<Lecture>();
        ArrayList<Lecture> lecturesOrganized = new ArrayList<Lecture>();
        Lecture l1 = new Lecture(
                "Ruby on Rails: Por que devemos deixá-lo para trás",
                Duration.ofMinutes(60));
        Lecture l2 = new Lecture(
                "Clojure engoliu Scala: migrando minha aplicação",
                Duration.ofMinutes(45));
        Lecture l3 = new Lecture(
                "Ensinando programação nas grotas de Maceió",
                Duration.ofMinutes(30));

        lecturesToOrganize.add(l3);
        lecturesToOrganize.add(l2);
        lecturesToOrganize.add(l1);

        lecturesToOrganize2.add(l3);
        lecturesToOrganize2.add(l1);
        lecturesToOrganize2.add(l2);

        lecturesOrganized.add(l1);
        lecturesOrganized.add(l2);
        lecturesOrganized.add(l3);

        SortLectures.sortLecturesByDescTime(lecturesToOrganize);
        SortLectures.sortLecturesByDescTime(lecturesToOrganize2);

        assertEquals(lecturesOrganized, lecturesToOrganize);
        assertEquals(lecturesOrganized, lecturesToOrganize2);
    }
}
