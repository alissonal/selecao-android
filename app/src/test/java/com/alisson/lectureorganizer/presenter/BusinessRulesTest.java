package com.alisson.lectureorganizer.presenter;

import android.util.Log;

import org.junit.Test;

import java.time.Duration;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class BusinessRulesTest {

    // A conferencia tem várias tracks, cada qual tendo uma sessão pela manhã e outra pela tarde.
    @Test
    public void doesConferenceHaveSeveralTracksTest() {
        assertEquals(true, (Conference.getTrackA() != null));
        assertEquals(true, (Conference.getTrackB() != null));
    }

    @Test
    public void doesTracksHaveMorningAndAfternoonSessionTest() {
        assertEquals(true, (Conference.getTrackA().getMorningSession() != null));
        assertEquals(true, (Conference.getTrackA().getAfternoonSession() != null));
        assertEquals(true, (Conference.getTrackB().getMorningSession() != null));
        assertEquals(true, (Conference.getTrackB().getAfternoonSession() != null));
    }

    // Sessões pela manhã começam às 9h e devem terminar às 12h, para o almoço.
    @Test
    public void doesMorningSessionsBeginAndEndAtTheDefinedTimeTest() {
        LocalTime morningSessionTrackAStartTime = LocalTime.of(Conference.getTrackA()
                .getMorningSession().START_MORNING_SESSION_TIME_IN_HOURS, 0);
        Duration morningSessionTrackADuration = Duration.ofMinutes(Conference.getTrackA()
                .getMorningSession().TOTAL_MORNING_SESSION_AVAILABLE_TIME_IN_MIN);
        LocalTime endMorningSessionTrackATime = morningSessionTrackAStartTime.plus(morningSessionTrackADuration);

        LocalTime morningSessionTrackBStartTime = LocalTime.of(Conference.getTrackB()
                .getMorningSession().START_MORNING_SESSION_TIME_IN_HOURS, 0);
        Duration morningSessionTrackBDuration = Duration.ofMinutes(Conference.getTrackB()
                .getMorningSession().TOTAL_MORNING_SESSION_AVAILABLE_TIME_IN_MIN);
        LocalTime endMorningSessionTrackBTime = morningSessionTrackBStartTime.plus(morningSessionTrackBDuration);

        assertEquals(true, (morningSessionTrackAStartTime.getHour() == 9 &&
                morningSessionTrackAStartTime.getMinute() == 0));
        assertEquals(true, (endMorningSessionTrackATime.getHour() == 12 &&
                endMorningSessionTrackATime.getMinute() == 0));
        assertEquals(true, (morningSessionTrackBStartTime.getHour() == 9 &&
                morningSessionTrackBStartTime.getMinute() == 0));
        assertEquals(true, (endMorningSessionTrackBTime.getHour() == 12 &&
                endMorningSessionTrackBTime.getMinute() == 0));
    }

    @Test
    public void doesLunchBeginAndEndAtDefinedTimeTest() {
        LocalTime lunchStartTime = LocalTime.of(12,00);
        LocalTime lunchEndTime = LocalTime.of(13,00);
        assertEquals(true, (Track.lunchStartTime == lunchStartTime));
        assertEquals(true,
                (Track.lunchStartTime.plus(Track.lunchTimeDuration) == lunchEndTime));
    }

    // Sessões pela tarde começam às 13h e devem terminar
    // a tempo de realizar o evento de networking.
    @Test
    public void doesAfternoonSessionsBeginAndEndAtTheDefinedTimeTest() {
        LocalTime afternoonSessionTrackAStartTime = LocalTime.of(Conference.getTrackA()
                .getAfternoonSession().START_AFTERNOON_SESSION_TIME_IN_HOURS, 0);
        Duration afternoonSessionTrackADuration = Duration.ofMinutes(Conference.getTrackA()
                .getAfternoonSession().TOTAL_AFTERNOON_SESSION_AVAILABLE_TIME_IN_MIN);
        LocalTime endAfternoonSessionTrackATime =
                afternoonSessionTrackAStartTime.plus(afternoonSessionTrackADuration);

        LocalTime afternoonSessionTrackBStartTime = LocalTime.of(Conference.getTrackB()
                .getAfternoonSession().START_AFTERNOON_SESSION_TIME_IN_HOURS, 0);
        Duration afternoonSessionTrackBDuration = Duration.ofMinutes(Conference.getTrackB()
                .getAfternoonSession().TOTAL_AFTERNOON_SESSION_AVAILABLE_TIME_IN_MIN);
        LocalTime endAfternoonSessionTrackBTime =
                afternoonSessionTrackBStartTime.plus(afternoonSessionTrackBDuration);

        LocalTime networkingTime = Track.networkingStartTime;

        assertEquals(true, (afternoonSessionTrackAStartTime.getHour() == 13 &&
                afternoonSessionTrackAStartTime.getMinute() == 0));
        assertEquals(false, (endAfternoonSessionTrackATime.isAfter(networkingTime)));
        assertEquals(true, (afternoonSessionTrackBStartTime.getHour() == 13 &&
                afternoonSessionTrackBStartTime.getMinute() == 0));
        assertEquals(false, (endAfternoonSessionTrackBTime.isAfter(networkingTime)));
    }

    // O evento de networking deve começar depois das 16h, mas antes das 17h.
    @Test
    public void doesNetworkingBeginAfterAndBeforeDefinedTimeTest() {
        LocalTime networkingTime = Track.networkingStartTime;
        LocalTime fourOClock = LocalTime.of(16,00);
        LocalTime fiveOClock = LocalTime.of(17,00);
        assertEquals(true, networkingTime.isAfter(fourOClock));
        assertEquals(true, networkingTime.isBefore(fiveOClock));
    }
}
