package com.alisson.lectureorganizer.presenter;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class BusinessRulesInstrumentedTest {
    private static Context appContext;

    @BeforeClass
    public static void startBusinessRulesInstrumentedTest() {
        appContext = InstrumentationRegistry.getTargetContext();
        Conference.startUp(appContext);
        Conference.organizeConference();
    }

    // Cada sessão contém várias palestras.
    @Test
    public void doesSessionsHaveSeveralLecturesTest() {
        assertEquals(true, (
                Conference.getTrackA().getMorningSession().getLecturesSession().size() > 1));
        assertEquals(true, (
                Conference.getTrackA().getAfternoonSession().getLecturesSession().size() > 1));
        assertEquals(true, (
                Conference.getTrackB().getMorningSession().getLecturesSession().size() > 1));
        assertEquals(true, (
                Conference.getTrackB().getAfternoonSession().getLecturesSession().size() > 1));
    }

    // Nenhum dos nomes das palestras possui números.
    @Test
    public void doesLecturesHaveNumbersInTitleTest() {
        boolean doesStringHaveNumber = false;
        for(Lecture l : Conference.getLecturesToOrganize()) {
            if(l.getTitle().matches(".*\\d+.*")) {
                doesStringHaveNumber = true;
                break;
            }
        }
        assertEquals(false, doesStringHaveNumber);
    }

    // A duração de todas as palestras são fornecidas em minutos ou
    // definidas como lightning (palestras de 5 minutos).
    @Test
    public void doesAllLecturesDurationCanBeRetrievedInMinutes() {
        boolean canRetrieveInMin = true;
        for(Lecture l : Conference.getLecturesToOrganize()) {
            try {
                if (l.getDurationTimeInMin() < 0) {
                    canRetrieveInMin = false;
                }
            } catch(Exception e) {
                canRetrieveInMin = false;
            }
        }
        assertEquals(true, canRetrieveInMin);
    }

    @Test
    public void doesLunchBeginAtDefinedTimeTest() {
        LocalTime lunchTime = LocalTime.of(12,00);
        assertEquals(true, (Conference.getTrackA().getLunch().startTime == lunchTime));
        assertEquals(true, (Conference.getTrackB().getLunch().startTime == lunchTime));
    }

    // O evento de networking deve começar depois das 16h, mas antes das 17h.
    @Test
    public void doesNetworkingBeginAfterAndBeforeDefinedTimeTest() {
        LocalTime networkingBeginAfterTime = LocalTime.of(16,00);
        LocalTime networkingBeginBeforeTime = LocalTime.of(17,00);
        assertEquals(true,
                (Conference.getTrackA()
                        .getNetworking().startTime.isAfter(networkingBeginAfterTime )));
        assertEquals(true,
                (Conference.getTrackA()
                        .getNetworking().startTime.isBefore(networkingBeginBeforeTime )));
    }

    @Test
    public void doesLecturesFromSessionListBeginAtTheRightTimeTest() {
        LocalTime morningStartTime = LocalTime.of(9,0);
        LocalTime afternoonStartTime = LocalTime.of(13,0);

        List<Lecture> a = Conference.getTrackA().getMorningSession().getLecturesSession();
        List<Lecture> b = Conference.getTrackB().getMorningSession().getLecturesSession();
        List<Lecture> c = Conference.getTrackA().getAfternoonSession().getLecturesSession();
        List<Lecture> d = Conference.getTrackB().getAfternoonSession().getLecturesSession();

        boolean checkA = true;
        boolean checkB = true;
        boolean checkC = true;
        boolean checkD = true;

        if (!a.isEmpty() && a.size() > 0) {
            Lecture firstElement = a.get(0);
            checkA = (firstElement.getStartTime() == morningStartTime);
        } else {
            checkA = false;
        }
        if (!b.isEmpty() && b.size() > 0) {
            Lecture firstElement = b.get(0);
            checkB = (firstElement.getStartTime() == morningStartTime);
        } else {
            checkB = false;
        }
        if (!c.isEmpty() && c.size() > 0) {
            Lecture firstElement = c.get(0);
            checkC = (firstElement.getStartTime() == afternoonStartTime);
        } else {
            checkC = false;
        }
        if (!d.isEmpty() && d.size() > 0) {
            Lecture firstElement = d.get(0);
            checkD = (firstElement.getStartTime() == afternoonStartTime);
        } else {
            checkD = false;
        }

        assertEquals(true, checkA);
        assertEquals(true, checkB);
        assertEquals(true, checkC);
        assertEquals(true, checkD);
    }

    @Test
    public void doesLecturesFromSessionListEndBeforeTheRightTime() {
        LocalTime lunchTime = LocalTime.of(12,0);
        LocalTime networkingTime = LocalTime.of(16,59);

        List<Lecture> a = Conference.getTrackA().getMorningSession().getLecturesSession();
        List<Lecture> b = Conference.getTrackB().getMorningSession().getLecturesSession();
        List<Lecture> c = Conference.getTrackA().getAfternoonSession().getLecturesSession();
        List<Lecture> d = Conference.getTrackB().getAfternoonSession().getLecturesSession();

        boolean checkA = true;
        boolean checkB = true;
        boolean checkC = true;
        boolean checkD = true;


        if (!a.isEmpty() && a.size() > 0) {
            Lecture lastElement = a.get(a.size() - 1);
            checkA = (lastElement.getEndTime().isBefore(lunchTime) ||
                    lastElement.getEndTime() == lunchTime);
        } else {
            checkA = false;
        }
        if (!b.isEmpty() && b.size() > 0) {
            Lecture lastElement = b.get(b.size() - 1);
            checkB = (lastElement.getEndTime().isBefore(lunchTime)  ||
                    lastElement.getEndTime() == lunchTime);
        } else {
            checkB = false;
        }
        if (!c.isEmpty() && c.size() > 0) {
            Lecture lastElement = c.get(c.size() - 1);
            checkC = (lastElement.getEndTime().isBefore(networkingTime)  ||
                    lastElement.getEndTime() == networkingTime);
        } else {
            checkC = false;
        }
        if (!d.isEmpty() && d.size() > 0) {
            Lecture lastElement = d.get(d.size() - 1);
            checkD = (lastElement.getEndTime().isBefore(networkingTime)  ||
                    lastElement.getEndTime() == networkingTime);
        } else {
            checkD = false;
        }

        assertEquals(true, checkA);
        assertEquals(true, checkB);
        assertEquals(true, checkC);
        assertEquals(true, checkD);
    }
}
