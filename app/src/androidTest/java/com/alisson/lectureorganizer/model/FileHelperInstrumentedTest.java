package com.alisson.lectureorganizer.model;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class FileHelperInstrumentedTest {
    @Test
    public void isLecturesToOrganizeEmptyAfterStartup() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        String inputRawLectures = FileHelper.ReadFile(appContext);
        assertEquals(false, inputRawLectures.isEmpty());
    }
}

