package com.alisson.lectureorganizer.model;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileHelper {
    private final static String fileName = "proposals.txt";
    private final static String TAG = FileHelper.class.getName();

    /**
     *  Read a file, at this point it can only read the filename which is hard coded
     *
     * @param context context from Activity
     * @return        the file with lines in a String
     */
    public static String ReadFile(Context context) {
        String line = null;

        try {
            InputStreamReader inputStreamReader = new InputStreamReader(
                    context.getAssets().open(fileName), "UTF-8");

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ( (line = bufferedReader.readLine()) != null )
            {
                stringBuilder.append(line + System.getProperty("line.separator"));
            }
            line = stringBuilder.toString();

            bufferedReader.close();
            inputStreamReader.close();
        } catch(FileNotFoundException ex) {
            Log.d(TAG, ex.getMessage());
        } catch(IOException ex) {
            Log.d(TAG, ex.getMessage());
        }
        return line;
    }
}
