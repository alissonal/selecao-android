package com.alisson.lectureorganizer.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.alisson.lectureorganizer.R;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Called when the user taps the Send button
    public void sendMessage(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, LectureActivity.class);
        startActivity(intent);
    }

}
