package com.alisson.lectureorganizer.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.alisson.lectureorganizer.presenter.Conference;
import com.alisson.lectureorganizer.R;

public class LectureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lectury);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.textView);

        textView.setMovementMethod(new ScrollingMovementMethod());

        Conference.startUp(this);
        if(Conference.organizeConference())
            textView.setText(Conference.getInstance().toString());
    }
}
