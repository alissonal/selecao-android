package com.alisson.lectureorganizer.presenter;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Event {
    protected Duration durationTime;
    protected LocalTime startTime = LocalTime.of(0, 0);
    protected String lectureTitle;
    //private LocalTime endTime = LocalTime.of(0, 0);

    private final static String TAG = Event.class.getName();

    public Event(){}

    /**
     * A constructor with a Title and a Start Time for the Event
     * @param lectureTitle
     * @param startTime
     */
    Event(String lectureTitle, LocalTime startTime) {
        this.lectureTitle = lectureTitle;
        this.startTime = startTime;
    }

    /**
     *
     * @param duration
     */
    void setDurationTime(Duration duration) {
        this.durationTime = duration;
    }

    /**
     *
     * @param startTime
     */
    void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     */
    LocalTime getStartTime() {
        return this.startTime;
    }

    /**
     *
      * @return
     */
    LocalTime getEndTime() {
        return startTime.plus(durationTime);
    }

    /**
     *
     * @return
     */
    String getTitle() {
        return this.lectureTitle;
    }

    /**
     *
     * @return
     */
    long getDurationTimeInMin() {
        return this.durationTime.toMinutes();
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return startTime.format(DateTimeFormatter.ofPattern("HH:mm")).toString()
                + " " + lectureTitle;
    }
}
