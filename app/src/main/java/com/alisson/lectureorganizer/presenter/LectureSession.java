package com.alisson.lectureorganizer.presenter;

import android.util.Log;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class LectureSession {

    static final int TOTAL_MORNING_SESSION_AVAILABLE_TIME_IN_MIN = 180;

    // It would be 240 if it weren't for the rule bussiness which say that networking must
    // happen before 17:00
    static final int TOTAL_AFTERNOON_SESSION_AVAILABLE_TIME_IN_MIN = 239;
    static final int START_MORNING_SESSION_TIME_IN_HOURS = 9;
    static final int START_AFTERNOON_SESSION_TIME_IN_HOURS = 13;

    //Current is TOTAL_MORNING_SESSION_AVAILABLE_TIME_IN_MIN or
    // TOTAL_AFTERNOON_SESSION_AVAILABLE_TIME_IN_MIN?
    private int maxSessionTimeInMin = 0;

    private static final String MORNING = "morning";
    private static final String AFTERNOON = "afternoon";

    // Morning or Afternoon?
    private String dayPeriod;

    private List<Lecture> lecturesSession;

    private final static String TAG = LectureSession.class.getName();

    private LocalTime startTime = LocalTime.of(0, 0);
    private LocalTime endTime = LocalTime.of(0, 0);

    /**
     *
     * @param dayPeriod
     */
    LectureSession(String dayPeriod) {
        lecturesSession = new ArrayList<Lecture>();

        // ToDo: Fazer checagem se dayPeriod foi digitado corretamente
        this.dayPeriod = dayPeriod;

        if(isMorningSession()) {
            maxSessionTimeInMin = TOTAL_MORNING_SESSION_AVAILABLE_TIME_IN_MIN;

            // ToDO: Fazer verificação se possui minutos
            startTime = LocalTime.of(START_MORNING_SESSION_TIME_IN_HOURS, 0);
        } else if (isAfternoonSession()) {
            maxSessionTimeInMin = TOTAL_AFTERNOON_SESSION_AVAILABLE_TIME_IN_MIN;

            // ToDO: Fazer verificação se possui minutos
            startTime = LocalTime.of(START_AFTERNOON_SESSION_TIME_IN_HOURS, 0);
        }
    }

    /**
     *
     * @return
     */
    private boolean isMorningSession() {
        return (dayPeriod.equals(MORNING))?true:false;
    }

    /**
     *
     * @return
     */
    private boolean isAfternoonSession() {
        return (dayPeriod.equals(AFTERNOON))?true:false;
    }

    /**
     * Move lectures from lectures to session,
     * removes then from the original array at the end
     * @param lectures
     */
    void moveLecturesToSession(List<Lecture> lectures) {
        // It's the final countdown!!
        int timeCountDown = maxSessionTimeInMin;

        for(Lecture l : lectures) {
            if(l.getDurationTimeInMin() <= timeCountDown) {
                lecturesSession.add(l);
                timeCountDown -= l.getDurationTimeInMin();
            }
        }

        lectures.removeAll(lecturesSession);
        setLecturesStartTime();
    }

    /**
     * Take the end time of the last lecture and put it at the start of the next one
     */
    private void setLecturesStartTime() {
        LocalTime endTimeLast = LocalTime.of(0, 0);
        boolean isFirst = true;
        for (Lecture l : lecturesSession) {
            if(isFirst) {
                isFirst = false;
                l.setStartTime(this.startTime);
                endTimeLast = l.getEndTime();
            } else {
                l.setStartTime(endTimeLast);
                endTimeLast = l.getEndTime();
            }
        }
    }

    //public void copySessionLecturesToList(List<Lecture> lectures) {
    //    lectures.addAll(lecturesSession);
    //}

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String buffer = "";
        //if(isMorningSession()) {
             //buffer += "Sessão da Manhã";
        //} else if(isAfternoonSession()) {
            //buffer += "Sessão da Tarde";
        //}

        //buffer += "\n";
        for(Lecture l : this.lecturesSession) {
            buffer += l.getStartTime().format(DateTimeFormatter.ofPattern("HH:mm")).toString() + " " + l.getTitle();
            buffer += "\n";
        }
        return buffer;
    }

    /**
     * For Debug Only
     */
    public void printDebugSession() {
        System.out.println("Sessão: "+ dayPeriod);
        for(Lecture l : this.lecturesSession) {
            System.out.println(l.getTitle()+" "+l.getDurationTimeInMin());
        }
        System.out.println("Fim da sessão");
    }

    /**
     *
     * @return
     */
    List<Lecture> getLecturesSession() {
        return lecturesSession;
    }
}
