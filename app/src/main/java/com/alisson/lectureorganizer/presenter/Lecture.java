package com.alisson.lectureorganizer.presenter;

import android.util.Log;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * At this point Lecture inherits from Event just to make it clear the conceptual
 * difference between than. Also it implements Comparable so it easy things for
 * the sort algorithm
 */
public class Lecture extends Event implements Comparable<Lecture> {

    private final static String TAG = Lecture.class.getName();

    /**
     * A constructor with a title and duration for a Lecture
     * @param lectureTitle
     * @param duration
     */
    Lecture(String lectureTitle, Duration duration) {
        this.lectureTitle = lectureTitle;
        this.durationTime = duration;
        //setDurationTime(duration);
    }

    /**
     * Compare in descending order
     * @param lecture
     * @return
     */
    @Override
    public int compareTo(Lecture lecture) {
        long compareQuantity = ((Lecture) lecture).getDurationTimeInMin();
        return (int)(compareQuantity - this.durationTime.toMinutes());
    }
}
