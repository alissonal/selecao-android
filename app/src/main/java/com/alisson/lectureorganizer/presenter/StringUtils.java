package com.alisson.lectureorganizer.presenter;

public class StringUtils {
    /**
     * Check if there is a number at a String
     * @param testString
     * @return
     */
    public static boolean doesStringHaveNumbers(String testString) {
        if(testString.matches(".*\\d+.*")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Split Lines
     * @param input
     * @return
     */
    public static String[] splitLines(String input) {
        return input.split("[\\r\\n]+");
    }

    /**
     * Take the last word and divide a String in two
     * @param input a String
     * @return      a array of two Strings
     */
    public static String[] separateLastWordFromString(String input) {
        int i = input.lastIndexOf(" ");
        String[] a = {input.substring(0, i), input.substring(i + 1)};
        return a;
    }
}
