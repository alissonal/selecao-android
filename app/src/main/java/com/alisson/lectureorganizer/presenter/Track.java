package com.alisson.lectureorganizer.presenter;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Track {
    private LectureSession morningSession;
    private LectureSession afternoonSession;

    private static final int TOTAL_TRACK_AVAILABLE_TIME_IN_MIN = 419;
    private static final int START_TRACK_TIME_IN_HOURS = 9;
    private static final double END_TRACK_TIME_IN_HOURS_BEFORE_NETWORKING = 16.98;
    private static final int START_NETWORKING_TIME = 17;

    static final LocalTime lunchStartTime = LocalTime.of(12, 0);
    static final Duration lunchTimeDuration = Duration.ofMinutes(60);
    static final LocalTime networkingStartTime = LocalTime.of(16, 59);
    private static final String lunchTitle = "Hora do Almoço";
    private static final String networkingTitle = "Networking";

    /**
     * I could have made lunch and networking as Lectures instead of making
     * Lecture inherit from Event, but this way it separates then conceptually
     */
    Event lunch;
    Event networking;

    private String trackTitle = "";

    /**
     *
     * @param trackTitle
     */
    Track(String trackTitle) {
        this.trackTitle = trackTitle;
        morningSession = new LectureSession("morning");
        afternoonSession = new LectureSession("afternoon");
        lunch = new Event(lunchTitle, lunchStartTime);
        lunch.setDurationTime(lunchTimeDuration);
        networking = new Event(networkingTitle, networkingStartTime);
    }

    /**
     * After it moves, it will remove all from the original list
     * @param lectures
     */
    void moveLecturesToTrack(List<Lecture> lectures) {
        morningSession.moveLecturesToSession(lectures);
        afternoonSession.moveLecturesToSession(lectures);
    }

    //void copyTrackToList(List<Lecture> lectures) {
    //    morningSession.copySessionLecturesToList(lectures);
    //    afternoonSession.copySessionLecturesToList(lectures);
    //}

    /*List<Event> getFullTrack() {
        List<Event> l = new ArrayList<Event>();

        l.addAll(morningSession.getLecturesSession());
        l.add(lunch);
        l.addAll(afternoonSession.getLecturesSession());
        l.add(networking);

        return l;
    }*/

    /**
     *
     * @return
     */
    Event getLunch() {
        return lunch;
    }

    /**
     *
     * @return
     */
    Event getNetworking() {
        return networking;
    }

    /**
     *
     * @return
     */
    LectureSession getMorningSession() {
        return morningSession;
    }

    /**
     *
     * @return
     */
    LectureSession getAfternoonSession() {
        return afternoonSession;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String buffer = trackTitle;
        buffer += "\n";
        buffer += "\n";
        buffer += morningSession.toString();
        buffer += lunch.toString();
        buffer += "\n";
        buffer += afternoonSession.toString();
        buffer += networking.toString();
        buffer += "\n";
        return buffer;
    }

    /**
     * For debug only
     */
    public void printDebugTrack() {
        System.out.println("Track: "+ this.trackTitle);
        morningSession.printDebugSession();
        afternoonSession.printDebugSession();
        System.out.println("Fim da track");
    }
}
