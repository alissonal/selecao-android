package com.alisson.lectureorganizer.presenter;

import java.util.Collections;
import java.util.List;

public class SortLectures {
    /**
     * It uses the compareTo override from lectures in a descending order
     * @param lecturesToOrganize
     */
    public static void sortLecturesByDescTime(List<Lecture> lecturesToOrganize) {
        Collections.sort(lecturesToOrganize);
    }
}
