package com.alisson.lectureorganizer.presenter;

import android.content.Context;
import android.util.Log;

import com.alisson.lectureorganizer.model.FileHelper;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Conference is a Singleton because this will only have one conference,
 * If it had others it should be refactored
 */
public class Conference {

    private static Track trackA;
    private static Track trackB;

    private static List<Lecture> lecturesToOrganize;

    private final static String TAG = Conference.class.getName();

    // This number depends on the numbers of algorithms to try to organize the lectures
    private static int attemptsToOrganizeConferenceRemaining = 1;

    private static final int LIGHTNING_LECTURE_IN_MINUTES = 5;
    private static final String LIGHTNING = "lightning";

    private static final Conference ourInstance = new Conference();

    /**
     * Return instance of singleton
     * @return
     */
    public static Conference getInstance() {
        return ourInstance;
    }

    /**
     * First it reads the txt file and then it calls setupLectures to put it in a List<Lecture>
     * @param context From Activity
     */
    public static void startUp(Context context) {
        // Read Conference File
        String inputRawLectures = FileHelper.ReadFile(context);
        setupLectures(inputRawLectures);
    }

    /**
     *
     */
    private Conference() {
        trackA = new Track("Track A");
        trackB = new Track("Track B");

        lecturesToOrganize = new ArrayList<>();
    }

    /**
     * Take the string file version with Lectures and put it in an ArrayList<Lecture>
     * @param inputLectureFile
     */
    private static void setupLectures(String inputLectureFile) {
        try {
            // Read the string file and split the lines
            String lines[] = StringUtils.splitLines(inputLectureFile);

            // Lê cada linha e separa em título de palestra e horário
            // No fim, cria o objeto da classe Lecture e adiciona
            // as informações no construtor

            /**
             *  Read each line and split lecture title from schedule time
             *  At the end it creates an object from Lecture and added the informations
             *  in his constructor
             */
            for (String o : lines) {
                String[] a = StringUtils.separateLastWordFromString(o);
                if(a[0] != null && a[1] != null) {
                    if(StringUtils.doesStringHaveNumbers(a[0])) {
                        Log.d(TAG, "Error in File, Found a number at Title");
                        return;
                    }

                    // Check if it is Lightning lecture
                    if(a[1].equals(LIGHTNING)) {
                        a[1] = a[1].replace(LIGHTNING, Integer.toString(LIGHTNING_LECTURE_IN_MINUTES));
                    } else {
                        try {
                            a[1] = a[1].replace("min", "");
                        } catch (Exception e) {
                            Log.d(TAG, e.getMessage());
                        }
                    }

                    // Add the lecture at the unsorted list of Lectures
                    try {
                       lecturesToOrganize.add(new Lecture(a[0],
                                Duration.ofMinutes(Long.parseLong(a[1]))));
                    } catch (NumberFormatException ne) {
                        Log.d(TAG, ne.getMessage());
                    } catch (Exception e) {
                        Log.d(TAG, e.getMessage());
                    }
                } else {
                    Log.d(TAG, "Empty string from file.");
                    return;
                }
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    /**
     * It tries two algorithms to organize, if one fails it tries the other one
     * @return
     */
    public static boolean organizeConference() {
        List<Lecture> lecturesTemp = new ArrayList<Lecture>(lecturesToOrganize);

        trackA.moveLecturesToTrack(lecturesTemp);
        trackB.moveLecturesToTrack(lecturesTemp);

        if(!lecturesTemp.isEmpty()) {
            try {
                Log.d(TAG, "Lectures remained not putted on schedule.");
            } catch (Exception e) {/*Ignore*/}
            if(attemptsToOrganizeConferenceRemaining > 0) {
                SortLectures.sortLecturesByDescTime(lecturesToOrganize);
                attemptsToOrganizeConferenceRemaining--;
                return organizeConference();
            } else {
                try {
                    Log.d(TAG, "Failed to organize conference.");
                } catch (Exception e) {/*Ignore*/}
                return false;
            }
        } else {
            // Lectures
            //trackA.copyTrackToList(lecturesToOrganize);
            //trackB.copyTrackToList(lecturesToOrganize);

            return true;
        }
    }

    /**
     *
     * @param _lecturesToOrganize
     */
    static void setLecturesToOrganize(List<Lecture> _lecturesToOrganize) {
        lecturesToOrganize = _lecturesToOrganize;
    }

    /**
     *
     * @return
     */
    public static Track getTrackA() {
        return trackA;
    }

    /**
     *
     * @return
     */
    public static Track getTrackB() {
        return trackB;
    }

    /**
     *
     * @return
     */
    static List<Lecture> getLecturesToOrganize() {
        return lecturesToOrganize;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String buffer = "Conferência";
        buffer += "\n";
        buffer += "\n";
        buffer += trackA.toString();
        buffer += "\n";
        buffer += trackB.toString();
        return buffer;
    }

    /**
     * For debug purpose
     */
    public static void printDebugConference() {
        System.out.println("Inicio da Conferencia");
        trackA.printDebugTrack();
        trackB.printDebugTrack();
        System.out.println("Fim da conferencia");
    }
}
